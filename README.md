# README #

### Getting Setup ###

First, click clone on the upper right corner of the bitbucket repository and then copy the git clone command.

In your own projects folder, you will paste the git clone command in order to create a copy of this repository that you can directly edit and execute.

Once you have cloned the repository, you will run the following script to set up the virtual environment (uses python3):

```sh ./setup_venv.sh```

You will then activate the virtual environment:

```source ./venv_loop_calling/bin/activate```




### Loop Calling Pipeline ###
Prior to running this code, raw Hi-C matrices should be balanced. Balanced matrices and their corresponding bias vectors are used as input files. 

Code should be run in the following order 1) expected.py, 2) pvalues.py, 3) qvalues.py, and then 4) clusters.py.

Example commands are provided inside the individual scripts. Each command requires that the Hi-C matrix resolution be specified. For wrapper scripts - this can be specified in the header near the top of the script. Paths to file directories are also specified in the top header. It is necessary to run all chromosomes as input to the q-value step for it to run successfully. 
**Recommended parameters settings for Hi-C 2.5 in human H1 ES and HCT116 cells are provided in the recommended_parameter_settings.txt file.**

**1) expected.py computes a donut expected background model to be used for p-value calling based on the [Rao et al. 2014 approach](https://www.cell.com/cell/fulltext/S0092-8674(14)01497-4?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0092867414014974%3Fshowall%3Dtrue#fig3).** 
The expectedwrapper.py script can be used to run code for all chromosomes. 

inputs: upper triangle chromosomal matrix containing balanced values. This matrix should be an npz file. 

outputs: An upper triangle chromosomal matrix containing expected values. Saves as an npz file. 


**2) pvalues.py calls a right-tailed p-value for each balanced pixel based on a poisson model parameterized by that pixel's expected background value.** 
The pvalueswrapper.py script can be used to run code for all chromosomes. 

inputs: upper triangle chromosomal matrix containing balanced values, expected values (output from step 1), and bias vectors (obtained from balancing raw Hi-C matrix)

outputs: upper triangle chromosomal matrix containing p-values. Saves as an npz file. 


**3) qvalues.py will perform BH FDR multiple testing correction on the expected value across all chromosomes.** 
The qvalueswrapper.py script can be used to run code for all chromosomes. 

inputs: upper triangle chromosomal matrix containing balanced values, expected values (output from step 1), p-value values (output from step 2), and bias vectors (obtained from balancing raw Hi-C matrix)

outputs: upper triangle chromosomal matrix containing q-values. Saves as an npz file. 


**4) clusters.py will take all of the q-values, find the significant ones, and then cluster them by adjacency.** 
The clusterswrapper.py script can be used to run code for all chromosomes. 

inputs: upper triangle chromosomal matrix containing balanced values, expected values (output from step 1), and q-value values (output from step 3)

outputs: a JSON file and a tab separated values (tsv) file. the JSON file contains a single JSON object, which is a triple nested list of integers. The inner lists are all of length 2 and represent specific pixels of the heatmap for that chromosome in terms of their row and column coordinates. The outer middle lists can be of any length and represent groups of pixels that belong to the same cluster. The length of the outer list corresponds to the number of clusters on that chromosome. 

The tsv files contains the following information:
     
    1.  loop_id: written as two sets of coordinates separated by an underscore (e.g. chr1:10000-30000_chr1:100000-130000). 
	    The first part represents the upstream coordinates of the cluster and 
        the second part represents the downstream coordinates of the clusters. 
    2.  obs: the balanced value of the pixel with the smallest q-value within the cluster. 
        This pixel is known as the peak pixel. 
    3.	exp: The expected value of the peak pixel
    4.	q-value: The q-value of the peak pixel
    5.	cluster size: The number of significant pixels within the cluster
    6.	us_chrom: The chromosome of the upstream coordinates 
    7.	us_start: The start of the upstream coordinates
    8.	us_end: The end of the upstream coordinates
    9.	ds_chrom: The chromosome of the downstream coordinates
    10.	ds_start: The start of the downstream coordinates
    11.	ds_end: The end of the downstream coordinates
    12.	peak pixel: This describes the coordinates of the peak pixel. 
        It is written as a set of upstream and downstream coordinates similar to loop_id.






### Addendum ###
Most wrapper scripts rely on the Platform LSF job scheduler to execute each step across chromosomes in parallel. If your machine has access to another job scheduler instead, modify all wrapper scripts by replacing `bsub` with the appropriate command. If you do not have access to a job scheduler, remove the bsub portion from the command line (cmd) inside the script. Example commands are included inside the individual scripts (i.e. expected.py).