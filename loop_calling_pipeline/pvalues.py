import sys
import os

import numpy as np
import scipy.sparse as sparse

from j5c_pvalue_calling.distributions import call_pvalues
from hic_banded_matrix.sparse_matrix import SparseContactMatrix as SCM
from hic_banded_matrix.system import check_outdir

def main():
    """
    example command
    ------------------
    python pvalues.py 10000 ./output/assembled_10kb/merged/scaled/filtered/balanced/iPS_merged_chr22_balanced.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/iPS_merged_chr22_bias.txt ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/iPS_merged_chr22_exp.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/iPS_merged_chr22_pvalues.npz
    """

    resolution = int(sys.argv[1])
    input_observed_npz_file = sys.argv[2]
    input_bias_txt_file = sys.argv[3]
    input_expected_npz_file = sys.argv[4]
    output_pvalues_npz_file = sys.argv[5]

    if os.path.exists(output_pvalues_npz_file):
        print('p-value matrix already exists, skipping')
        sys.exit()
    
    obs_scm = SCM.load(input_observed_npz_file,max_range=int(10000000/resolution))
    if not obs_scm.is_upper():
        obs_scm.make_upper()
    exp_scm = SCM.load(input_expected_npz_file,max_range=int(10000000/resolution))
    if not exp_scm.is_upper():
        exp_scm.make_upper()
    
    bias = np.loadtxt(input_bias_txt_file)
    if bias.shape[0] >obs_scm.shape[0]:
        bias = bias[:obs_scm.shape[0]] 

 
    
    min_exp=0.5

    print('making pvalues')
    # check deconv
    print(' deconvoluting')
    
    # deconvolution makes it so that during pvalue calling, the raw value at a
    # pixel will be compared to a "biased expected" value created by adding back the bias
    # factors for that pixel into the expected value via simple multiplication
    obs_scm = obs_scm.deconvolute(bias)
    exp_scm = exp_scm.deconvolute(bias)

    # check clamp
    print(' clamping expected values')
    exp = SCM.apply(lambda x, y: np.where(x < y, y, x),
                            exp_scm, min_exp)

    # call pvalues
    print(' call p-values')
    pvalues_scm = SCM.apply(call_pvalues, obs_scm,
                            exp_scm, exp_scm, 'poisson', log=False)

    print(' saving pvalues')
    check_outdir(output_pvalues_npz_file)
    pvalues_scm.save(output_pvalues_npz_file)

if __name__ == '__main__':
    main()
