import subprocess

chroms =  ['chr%i'% i for i in range(1,23)] + ['chrX']
reskb = 10 #kb
res = 10000
reps = ['H1hESC2.5']

#this saves as part of the name of the file. Can be changed to anything or taken out all together. It is typically used to include parameter values in the name of the file. 
para = 'p2w10_no_triu_VH_minsize_3_mindist_3_minobs_8_conn_1'

#this is the pathway only. The name of the file is hard coded below in the cmd line
balanced_path ='/pathway/to/balanced/files'
expected_path = '/pathway/to/expected/files'
pvalues_path = '/pathways/to/pvalues/files'
outputfile_path = '/pathway/where/you/want/to/save/qvalues/files'

for rep in reps:
    for chrom in chroms:
        cmd = f'bsub -M 80000 -o qval.log python qvalues.py 23 {res} {pvalues_path}/{rep}_{chrom}_pvalues_{para}.npz {expected_path}/{rep}_{chrom}_exp_{para}.npz {balanced_path}/{rep}_bias_{chrom}_{res}.txt {outputfile_path}/{rep}_{chrom}_qvalues_{para}.npz'
        print(cmd)
        subprocess.call(cmd, shell=True)
