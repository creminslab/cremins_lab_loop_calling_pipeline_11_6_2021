import subprocess

chroms = ['chr%i'% i for i in range(1,23)] + ['chrX']
reskb = 10 #kb
res = 10000
reps = ['H1hESC2.5']

#this saves as part of the name of the file. Can be changed to anything or taen out all together. It is typically used to include paramter vlaues in the name of the file. 
para =f'p2w10_no_triu_VH_minsize_3_mindist_3_minobs_8_conn_1'

#this is the pahtway only. The name of the file is hard coded below in the cmd line
balanced_path ='/pathway/to/balanced_and_bias/files' 
expected_path = '/pathway/to/expected/files'
outputfile_path = '/pathway/where/you/want/to/save/pvalues/files'

for rep in reps:
    for chrom in chroms:
        cmd = f'bsub -M 50000 -o pval.log python pvalues.py {res} {balanced_path}/{rep}_balanced_{chrom}_{res}.npz {balanced_path}/{rep}_bias_{chrom}_{res}.txt {expected_path}/{rep}_{chrom}_exp_{para}.npz {outputfile_path}/{rep}_{chrom}_pvalues_{para}.npz'
        print(cmd)
        subprocess.call(cmd, shell=True)

