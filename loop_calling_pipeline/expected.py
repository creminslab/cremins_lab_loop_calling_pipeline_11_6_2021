import sys
import os
import scipy.sparse as sparse

from hic_expected_modeling.exp import dynamic_filter_split, make_one_dim_exp, fuse_matrices 
from hic_expected_modeling.sparse_matrix import SparseContactMatrix as SCM

def main():

    """
    example command
    ------------------
    python expected.py 10000 ./output/assembled_10kb/merged/scaled/filtered/balanced/iPS_merged_chr22_balanced.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/iPS_merged_chr22_exp.npz 
    """

    resolution = int(sys.argv[1])
    input_observed_npz_file = sys.argv[2]
    output_expected_npz_file = sys.argv[3]
    
    if os.path.exists(output_expected_npz_file):
        print('expected matrix already exists, skipping')
        sys.exit()

    # the inner radius of the donut
    p=2
    # the outer radius of the donut is controlled by these three parameters
    # this code will try all outer donut sizes w between min_w and max_w (inclusive),
    # and in the end it will use the smallest w for which the sum of the values in
    # the lower left footprint is greater than min_lower_left_sum
    # this approach is borrowed directly from Rao et al. 2014
    # in practice, we set min_w equal to max_w to turn off this dynamic w selection
    min_w=10
    max_w=10
    min_lower_left_sum=32
    # if fewer than this fraction of the pixels in any footprint are nonzero,
    # this point will not get an expected value and will be excluded from loop calling
    # this setting is important for reducing false positive near unmappable regions
    min_filter_fraction=0.25
    # if include triu is set to True, expected model fusion will switch from the 
    # "normal" expected model, which is exp = max(donut, lower_left), to an upper 
    # triangle expected model for interactions within fuse_range pixels of the diagonal. 
    # Note that this upper triangle expected model uses a different set of p and
    # w parameters. 
    include_triu=False
    p_triu=2
    min_w_triu=10
    max_w_triu=10
    #fusion range for triangle is in bins. bins*resolution = fusion range in kb
    fuse_range=20
    
    include_vert_horz = True

    obs_scm = SCM.load(input_observed_npz_file,max_range=int(10000000/resolution))
    if not obs_scm.is_upper():
        obs_scm.make_upper()

    print('making exp')
    print(' computing 1D expected model')
    one_dim_exp_scm = SCM.apply(make_one_dim_exp, obs_scm)

    print(' computing donut expected')
    exp_scms = list(SCM.apply(
        dynamic_filter_split,
        obs_scm, one_dim_exp_scm,
        p=p,
        w_range=(min_w, max_w + 1, 1),
        p_triu=p_triu,
        w_triu_range=(min_w_triu,max_w_triu + 1, 1),
        min_ll=min_lower_left_sum,
        min_fraction=min_filter_fraction,
        include_triu=include_triu, 
        include_vert_horz=include_vert_horz
    ))

    if include_triu and include_vert_horz:
        exp_donut_scm, exp_ll_scm, short_exp_scm, vert_exp_scm, horz_exp_scm = exp_scms[0:5]
    elif include_triu and not include_vert_horz:
        exp_donut_scm, exp_ll_scm, short_exp_scm = exp_scms[0:3]
    elif include_vert_horz and not include_triu:
        exp_donut_scm, exp_ll_scm, vert_exp_scm, horz_exp_scm = exp_scms[0:4]
    else:
        exp_donut, exp_ll_scm = exp_scms[0:2] 

    if include_vert_horz:
        exp_scm = SCM.max(exp_donut_scm, exp_ll_scm, vert_exp_scm, horz_exp_scm)
    else:
        exp_scm = SCM.max(exp_donut_scm, exp_ll_scm)
    
    if include_triu:
        exp_scm = SCM.apply(fuse_matrices, short_exp_scm, exp_scm,
                        k=fuse_range + 1)

    print(' saving')
    
    exp_scm.save(output_expected_npz_file)

if __name__ == '__main__':
    main()
