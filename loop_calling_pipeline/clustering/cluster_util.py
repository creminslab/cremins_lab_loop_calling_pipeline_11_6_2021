import numpy as np

from .clusters import find_clusters
from .sparse_matrix import SparseContactMatrix as SCM

def apply_thresholds(obs, pvalues, exp, min_obs=10.0, max_pvalue=0.001,
                     min_fc=1.5):
    """
    Applies specific thresholds to some ``np.ndarray``s, returning a boolean
    ``np.ndarray`` that has True at positions that pass all the thresholds.

    Parameters
    ----------
    obs, pvalues, exp : np.ndarray
        The arrays that the thresholds will be applied to. Should all have the
        same shape.
    min_obs, max_pvalue, min_fc : float
        The thresholds to apply.

    Returns
    -------
    np.ndarray
        A boolean array with True at positions that pass all the thresholds.
    """

    return np.where(
        (obs >= min_obs) &
        (pvalues <= max_pvalue) &
        ((obs / exp) >= min_fc),
        True, False)


def make_clusters(obs_scm, exp_scm, pvalues_scm, pvalue=0.001, min_obs=10.0,
                  min_fc=1.5, min_size=3, connectivity=1):
    sig_scm = SCM.apply(
        apply_thresholds,
        obs_scm, pvalues_scm, exp_scm,
        min_obs=min_obs,
        max_pvalue=pvalue,
        min_fc=min_fc
    )
    clusters = find_clusters(sig_scm, connectivity=connectivity)
    return [c for c in clusters if len(c) >= min_size]


def merge_clusters(small, big):
    all_small = {pixel for cluster in small for pixel in cluster}
    return small + [cluster for cluster in big
                    if not any([pixel in all_small for pixel in cluster])]
