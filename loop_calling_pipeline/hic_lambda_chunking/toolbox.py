"""
Module for collecting all .hiclite subcommands for command line interaction.

Based on a combination of
https://gist.github.com/mivade/384c2c41c3a29c637cb6c603d4197f9f
and https://stackoverflow.com/a/25562415
"""

import argparse
import importlib
import pkgutil

import .hiclite
import .hiclite.tools


def import_submodules(package, recursive=True):
    """
    Import all submodules of a module.

    Parameters
    ----------
    package : str or module
        Package to import submodules of (name or actual module).
    recursive : bool
        Pass True to import submodules recursively.

    Returns
    -------
    dict
        A dict mapping the imported module names (as strings) to the modules
        themselves.
    """
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results


class CustomHelpFormatter(argparse.HelpFormatter):
    def _format_usage(self, usage, actions, groups, prefix):
        return ''

    def format_help(self):
        help = super(CustomHelpFormatter, self).format_help()
        return '\n'.join(filter(
            lambda x: not (x.startswith('positional') or x.startswith('  {')),
            help.split('\n')
        ))


root_parser = argparse.ArgumentParser(prog='.hiclite',
                                      formatter_class=CustomHelpFormatter)
subparsers = root_parser.add_subparsers(help='sub-commands', prog='.hiclite')
root_parser.add_argument(
        '-v', '--version',
        action='version',
        version='.hiclite version %s' % .hiclite.__version__)


def argument(*name_or_flags, **kwargs):
    """
    Convenience function to properly format arguments to pass to the subcommand
    decorator.
    """
    return [e for e in name_or_flags], kwargs


def subcommand(name=None, help='', args=(), parent=subparsers):
    """
    Decorator to define a new subcommand in a sanity-preserving way.
    The function will be stored in the ``func`` variable when the parser
    parses arguments so that it can be called directly like so::

        args = cli.parse_args()
        args.func(args)

    Usage example::

        @subcommand([argument("-d", help="debug mode", action="store_true")])
        def subcommand(args):
            print(args)

    Then on the command line::

        $ python cli.py subcommand -d

    """
    def decorator(func):
        resolved_name = name if name else func.__name__
        parser = parent.add_parser(resolved_name,
                                   prog='.hiclite %s' % resolved_name,
                                   help=help, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator


def main():
    import_submodules(.hiclite.tools)
    args = root_parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
