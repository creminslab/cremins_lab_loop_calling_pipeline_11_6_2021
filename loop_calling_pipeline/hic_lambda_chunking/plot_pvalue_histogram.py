import numpy as np
import matplotlib.pyplot as plt

from .plotting import plotter

from .filenames import format_outfile
from ..config import parse_config
from .sparse_matrix import SparseContactMatrix as SCM
from ..toolbox import subcommand, argument


@plotter
def plot_pvalue_histogram(pvalues, **kwargs):
    plt.hist(pvalues, bins=np.linspace(0, 1, 101))


@subcommand('plot_pvalue_histogram', 'plot histogram of called pvalues', [
    argument('rep'),
    argument('chrom'),
    argument('--base_outdir', default='heatmaps'),
    argument('--config', default='config.cfg')
])
def main(args):
    print('parsing config')
    config = parse_config(args.config)

    # load obs and pvalues
    stage = 'obs' if 'obs' in zip(*config.items('steps'))[0] else 'input'
    obs = SCM.load(format_outfile(config, args.rep, args.chrom, stage))
    pvalues = SCM.load(format_outfile(config, args.rep, args.chrom, 'pvalues'))
    min_obs = config.getfloat('clusters_params', 'min_obs')
    filtered_pvalues = pvalues.data[np.isfinite(pvalues.data) &
                                    (obs.data > min_obs)]

    # plot pvalue histogram
    outfile = '%s/%s_%s_pvalues.png' % (args.base_outdir, args.rep, args.chrom)
    plot_pvalue_histogram(filtered_pvalues, xlabel='pvalue',
                          ylabel='number of pixels', outfile=outfile)


if __name__ == '__main__':
    main()
