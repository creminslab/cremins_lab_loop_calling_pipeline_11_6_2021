import math

import numpy as np

#from .plot_pvalue_histogram import plot_pvalue_histogram
from .statistics import adjust_pvalues


BIN_SPACING = 2**(1./3)


def compute_lamdba_chunked_qvalues(pvalue_matrices, exp_matrices,
                                   plot_pattern=None):
    """
    Performs lambda-chunked BH p-value adjustment.

    Parameters
    ----------
    pvalue_matrices, exp_matrices: dict of np.ndarray
        Parallel dicts of matrices of p-values and expected values,
        respectively. Keys should be chromosome names as strings.
    plot_pattern : str, optional
        Pass a string pattern (containing two ``%s``'s; the first will be
        replaced with 'pvalues' or 'qvalues' and the second with the number of
        the chunk) to p-value distributions for each chunk to the specified
        output files.

    Returns
    -------
    dict of np.ndarray
        The adjusted p-values, in a dict parallel to the input dicts.
    """
    chrom_order = list(pvalue_matrices.keys())
    qvalue_matrices = {chrom: np.full_like(pvalue_matrices[chrom], np.nan)
                       for chrom in chrom_order}
    finite_idx = {chrom: (np.isfinite(pvalue_matrices[chrom]) &
                          np.isfinite(exp_matrices[chrom]))
                  for chrom in chrom_order}
    max_exp = max(np.max(exp_matrices[chrom][finite_idx[chrom]])
                  for chrom in chrom_order)
    n_bins = int(math.ceil(math.log(max_exp + 0.01, BIN_SPACING)))
    edges = np.concatenate([[0], BIN_SPACING ** np.arange(n_bins+1)])
    for i in range(n_bins):
        bin_idx = []
        pvalues_list = []
        sizes = []
        for chrom in chrom_order:
            idx = finite_idx[chrom] & (exp_matrices[chrom] >= edges[i]) & \
                (exp_matrices[chrom] < edges[i+1])
            bin_idx.append(idx)
            sizes.append(idx.sum())
            pvalues_list.append(pvalue_matrices[chrom][idx])
        slice_bounds = np.concatenate([[0], np.cumsum(sizes)])
        pvalues = np.concatenate(pvalues_list)
        if not np.any(pvalues):
            continue
        qvalues = adjust_pvalues(pvalues)
        for j in range(len(chrom_order)):
            qvalue_matrices[chrom_order[j]][bin_idx[j]] = qvalues[slice_bounds[j]:slice_bounds[j+1]]
 #       if plot_pattern:
 #           plot_pvalue_histogram(
 #               pvalues, outfile=plot_pattern % ('pvalues', i))
 #           plot_pvalue_histogram(
 #               qvalues, outfile=plot_pattern % ('qvalues', i))
    return qvalue_matrices
