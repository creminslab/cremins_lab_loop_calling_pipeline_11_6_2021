
module load python/3.9.1
python -m venv venv_loop_calling

source ./venv_loop_calling/bin/activate

pip install --upgrade pip
pip install numpy
pip install pybigwig
pip install backports.functools-lru-cache==1.6.1
pip install cycler==0.10.0
pip install decorator==4.4.1
pip install dill==0.3.1.1
pip install kiwisolver==1.1.0
pip install matplotlib==2.2.5
pip install pandas==0.24.2
pip install patsy==0.5.1
pip install pyparsing==2.4.6
pip install python-dateutil==2.8.1
pip install pytz==2019.3
pip install scipy==1.2.3
pip install seaborn==0.9.1
pip install six==1.14.0
pip install statsmodels==0.10.2
