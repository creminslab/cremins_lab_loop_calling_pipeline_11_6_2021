import sys
import os
import numpy as np
from hic_lambda_chunking.sparse_matrix import SparseContactMatrix as SCM
from hic_lambda_chunking.system import check_outdir
from hic_lambda_chunking.lambda_chunking import compute_lamdba_chunked_qvalues

def main():

    """
    example command
    --------------- 
    python qvalues.py 23 10000 ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/iPS_merged_chrom_pvalues.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/iPS_merged_chrom_exp.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/iPS_merged_chrom_bias.txt ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/qvalues/iPS_merged_chrom_qvalues.npz
    """
    
    number_of_chroms = int(sys.argv[1])
    resolution = int(sys.argv[2])
    input_pvalues_npz_pattern = sys.argv[3]
    input_expected_npz_pattern = sys.argv[4]
    input_bias_txt_file = sys.argv[5]
    output_qvalues_npz_pattern = sys.argv[6]
    
   

    chroms = ['chr%s'%i for i in range(1,number_of_chroms)] + ['chrX']
    for chrom in chroms:
        if not os.path.exists(input_pvalues_npz_pattern.replace('chrom',chrom)):
            print('You need all p-values files computed before computing q-values')
            sys.exit()

    print('making qvalues')
    pvalue_scms = {chrom: SCM.load(input_pvalues_npz_pattern.replace('chrom',chrom),max_range=int(10000000/resolution)) for chrom in chroms}
    exp_scms = {chrom: SCM.load(input_expected_npz_pattern.replace('chrom',chrom),max_range=int(10000000/resolution)) for chrom in chroms}
    qvalue_filenames = {chrom: output_qvalues_npz_pattern.replace('chrom',chrom) for chrom in chroms}


    for chrom in chroms:
        exp_scms[chrom] = exp_scms[chrom].deconvolute(np.loadtxt(input_bias_txt_file.replace('chrom',chrom)))
            
    print(' lambda chunking')
    pvalue_matrices = {chrom: pvalue_scms[chrom].data for chrom in chroms}
    exp_matrices = {chrom: exp_scms[chrom].data for chrom in chroms}

    # This performs a separate round of BH-FDR correction on each "chunk" of pixels
    # pixels are assigned to chunks based on their expected value
    qvalue_matrices = compute_lamdba_chunked_qvalues(pvalue_matrices, exp_matrices)
    
    print(' saving qvalues')
    for chrom in chroms:
        check_outdir(qvalue_filenames[chrom])
        pvalue_scms[chrom].data = qvalue_matrices[chrom]
        pvalue_scms[chrom].save(qvalue_filenames[chrom])

if __name__ == '__main__':
    main()
