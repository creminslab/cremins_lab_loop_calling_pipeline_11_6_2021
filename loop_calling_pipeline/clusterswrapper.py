import subprocess


chroms = ['chr%i'% i for i in range(1,23)] + ['chrX']
reskb = 10 #kb
res = 10000
reps = ['H1hESC2.5']

#this saves as part of the name of the file. Can be changed to anything or taken out all together. It is typically used to include parameter valeus in teh name of the file. 
para = 'p2w10_no_triu_VH_minsize_3_mindist_3_minobs_8_conn_1'

#this is the pathway only. The name of the file is hard coded below in the cmd line
balanced_path ='/pathway/to/balanced/files'
expected_path = '/pathway/to/expected/files'
qvalues_path = '/pathway/to/qvalues/files'
outputfile_path = '/pathway/where/you/want/to/save/clusters/files'

for rep in reps:
    for chrom in chroms:
        cmd = f'bsub -M 40000 -o clust.log python clusters.py {res} {chrom} {balanced_path}/{rep}_balanced_{chrom}_{res}.npz {expected_path}/{rep}_{chrom}_exp_{para}.npz {qvalues_path}/{rep}_{chrom}_qvalues_{para}.npz {outputfile_path}/{rep}_{chrom}_{para}.json {outputfile_path}/{rep}_{chrom}_loops_{para}.tsv'

        print(cmd)
        subprocess.call(cmd, shell=True)
