import sys

import os
from functools import reduce
from collections import OrderedDict


import numpy as np
import pandas as pd

from clustering.bed_util import parse_feature_from_string

from clustering.cluster_util import apply_thresholds, make_clusters, merge_clusters
from clustering.clusters import  save_clusters, cluster_to_loop_id, find_clusters
from clustering.sparse_matrix import SparseContactMatrix as SCM

def main():
    """
    example command
    ---------------
    python clusters.py 10000 chr22 ./output/assembled_10kb/merged/scaled/filtered/balanced/iPS_merged_chr22_balanced.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/iPS_merged_chr22_exp.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/qvalues/iPS_merged_chr22_qvalues.npz ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/qvalues/clusters/iPS_merged_chr22_clusters.json ./output/assembled_10kb/merged/scaled/filtered/balanced/expected/pvalues/qvalues/clusters/iPS_merged_chr22_loops.tsv
    """

    resolution = int(sys.argv[1])
    chrom = sys.argv[2]
    input_observed_npz_file = sys.argv[3]
    input_expected_npz_file = sys.argv[4]
    input_qvalues_npz_file = sys.argv[5]
    output_clusters_json_file = sys.argv[6]
    output_loops_tsv_file = sys.argv[7]
    
    if os.path.exists(output_clusters_json_file) and os.path.exists(output_loops_tsv_file):
        print('clusters already exist, skipping')
        print('loops already exist, skipping')
        sys.exit()
    
    obs_scm = SCM.load(input_observed_npz_file,max_range=int(10000000/resolution))
    if not obs_scm.is_upper():
        obs_scm.make_upper()
    exp_scm = SCM.load(input_expected_npz_file,max_range=int(10000000/resolution))
    if not exp_scm.is_upper():
        exp_scm.make_upper()
    qvalues_scm = SCM.load(input_qvalues_npz_file,max_range=int(10000000/resolution))
    if not qvalues_scm.is_upper():
        qvalues_scm.make_upper() 

    # the min_qvalue and max_qvalue parameters control the iterative cluster
    # refinement thresholds; briefly, we first threshold loops at max_qvalue,
    # then progressively "tighten" the qvalue threshold, reducing it towards min_qvalue
    # if a cluster of significant pixels becomes smaller when the threshold is
    # tightened, we keep the smaller, more refined footprint; if a cluster is
    # lost entirely when we tighten the threshold, we keep the footprint it had
    # before it was lost
    max_qvalue=0.025
    min_qvalue=1e-20
    # pixels with observed values smaller than min_obs will never be called significant
    min_obs=8
    # pixels with a fold change (obs/exp) less than min_fc will never be called significant
    min_fc=1.5
    # this parameter controls the definition of "adjacency" used when clustering
    # adjacent significant pixels; a value of 2 implies that diagonally-adjacent
    # pixels are considered to be adjacent, while a value of 1 implies the opposite
    connectivity=1
    # clusters with fewer than min_size significant pixels will be filtered out
    min_size=3
    # clusters which encroach within min_dist bin units of the diagonal will be filtered out
    min_dist=3

    print('making clusters')
    print(' thresholding')
    clusters = reduce(
        merge_clusters,
        (make_clusters(
            obs_scm, exp_scm, qvalues_scm, pvalue=qvalue,
            min_obs=min_obs,
            min_fc=min_fc,
            min_size=min_size,
            connectivity=connectivity
         )
         for qvalue in reversed(
            10 ** np.arange(np.log10(max_qvalue),
                            np.log10(min_qvalue)-1, step=-1)))
    )

    print(' distance filtering')
    clusters = [c for c in clusters if min([j-i for i, j in c]) >= min_dist]

    print(' saving clusters')
    save_clusters(clusters, output_clusters_json_file)

    print(' compiling loop information')
    loops = []
    for cluster in clusters:
        cluster = list(cluster)
        peak_pixel = cluster[np.argmin(qvalues_scm[tuple(zip(*cluster))])]
        loop_id = cluster_to_loop_id(
            cluster, chrom, resolution)
        us_anchor, ds_anchor = map(parse_feature_from_string,
                                   loop_id.split('_'))
        loops.append(OrderedDict([
            ('loop_id', loop_id),
            ('obs', float(obs_scm[peak_pixel])),
            ('exp', float(exp_scm[peak_pixel])),
            ('qvalue', float(qvalues_scm[peak_pixel])),
            ('cluster_size', len(cluster)),
            ('us_chrom', us_anchor['chrom']),
            ('us_start', us_anchor['start']),
            ('us_end', us_anchor['end']),
            ('ds_chrom', ds_anchor['chrom']),
            ('ds_start', ds_anchor['start']),
            ('ds_end', ds_anchor['end']),
            ('peak_pixel', cluster_to_loop_id(
                [peak_pixel], chrom,
                resolution))
        ]))
    if loops:
        df = pd.DataFrame(loops)
        df.set_index('loop_id', inplace=True)
        df.sort_values(by='us_start', inplace=True)

        print(' saving loop information')
        df.to_csv(output_loops_tsv_file, sep='\t')
    else:
        print(' no loops found, writing header line only')
        with open(output_loops_tsv_file, 'w') as handle:
            handle.write('loop_id\tobs\texp\tqvalue\tcluster_size\tus_chrom\t'
                         'us_start\tus_end\tds_chrom\tds_start\tds_end\t'
                         'peak_pixel\n')
if __name__ == '__main__':
    main()
