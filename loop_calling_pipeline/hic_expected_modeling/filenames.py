import os


STEP_TO_STAGE = {
    'assemble': ['raw'],
    'filter': ['filtered', 'mappability'],
    'balance': ['balanced', 'bias'],
    'obs': ['obs'],
    'exp': ['exp', 'exp_ll', 'exp_donut'],
    'var': ['var', 'obs_over_exp', 'fitting', 'disp'],
    'pvalues': ['pvalues'],
    'qvalues': ['qvalues'],
    'clusters': ['clusters', 'loops']
}


STAGE_TO_EXT = {
    'raw': 'npz',
    'filtered': 'npz',
    'mappability': 'txt',
    'balanced': 'npz',
    'bias': 'txt',
    'obs': 'npz',
    'exp': 'npz',
    'exp_ll': 'npz',
    'exp_donut': 'npz',
    'var': 'npz',
    'pvalues': 'npz',
    'qvalues': 'npz',
    'fitting': 'png',
    'clusters': 'json',
    'loops': 'tsv'
}


def format_outfile(config, rep, chrom, stage, ext=None):
    """
    Specifies the strategy for where to read and write files from.

    Every function that reads or writes an intermediate file should use this
    function to figure out where to read from/write to.

    Parameters
    ----------
    config : ConfigParser instance
        Used to look up information about pipeline parameters.
    rep, chrom, stage, ext : str
        Information about this specific file.

    Returns
    -------
    str
        The final filename.
    """
    # short-circuit to inject bias
    if stage == 'bias' and config.has_section('bias'):
        return config.get('bias', rep).replace('%c', chrom)

    # short circuit to inject input files
    if stage == 'input':
        return config.get('reps', rep).replace('%c', chrom)

    # resolve default extension
    if ext is None:
        ext = STAGE_TO_EXT[stage]

    # prepare filename
    filename = '%s_%s_%s.%s' % (rep, chrom, stage, ext)

    # assemble folder
    folder = 'plots' if ext == 'png' else 'output'
    for step, _ in config.items('steps'):
        if step == 'assemble':
            raw_dir = 'assembled_%skb' % \
                (config.getint('assemble_params', 'resolution') / 1000)
            folder = os.path.join(folder, raw_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step in ['filter', 'mappability']:
            filtered_dir = 'filtered_%s_%s_%s_%s_%s_%s_%s' % (
                config.getint('filter_params', 'sparse_count'),
                config.getint('filter_params', 'sparse_range'),
                config.getint('filter_params', 'zone_size'),
                config.getfloat('filter_params', 'map_threshold'),
                config.getint('filter_params', 'map_window'),
                config.getint('filter_params', 'outlier_size'),
                config.getfloat('filter_params', 'outlier_threshold')
            )
            folder = os.path.join(folder, filtered_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'balance':
            balanced_dir = 'balanced_%s' % \
                config.get('balance_params', 'method')
            folder = os.path.join(folder, balanced_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'obs':
            obs_dir = 'obs_%s' % config.getint('obs_params', 'max_range')
            folder = os.path.join(folder, obs_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'exp':
            exp_dir = 'exp_%s_%s_%s_%s_%s_%s_%s_%s' % (
                config.getint('exp_params', 'p'),
                config.getint('exp_params', 'min_w'),
                config.getint('exp_params', 'max_w'),
                config.getint('exp_params', 'min_lower_left_sum'),
                config.getint('exp_params', 'min_range'),
                config.getfloat('exp_params', 'min_filter_fraction'),
                config.getint('exp_params', 'fuse_range'),
                config.get('exp_params', 'fuse_model')
            )
            folder = os.path.join(folder, exp_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'var':
            var_dir = 'var_%s_%s_%s_%s_%s' % (
                config.getboolean('var_params', 'deconvolute'),
                config.get('var_params', 'method'),
                config.getfloat('var_params', 'min_exp'),
                config.getfloat('var_params', 'min_obs'),
                config.getint('var_params', 'min_dist')
            )
            folder = os.path.join(folder, var_dir)
            if stage in STEP_TO_STAGE[step]:
                # special override, there's only one var file for all reps
                if config.get('var_params', 'method') == 'deseq2':
                    filename = '%s_%s.%s' % (chrom, stage, ext)
                return os.path.join(folder, filename)
        elif step == 'pvalues':
            pvalues_dir = 'pvalues_%s' % config.get('pvalues_params', 'model')
            folder = os.path.join(folder, pvalues_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'qvalues':
            qvalues_dir = 'qvalues_%s' % \
                config.getboolean('qvalues_params', 'lambda_chunk')
            folder = os.path.join(folder, qvalues_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'clusters':
            clusters_dir = 'clusters_%s_%s_%s_%s_%s_%s_%s' % (
                config.getfloat('clusters_params', 'max_pvalue'),
                config.getfloat('clusters_params', 'min_pvalue'),
                config.getfloat('clusters_params', 'min_obs'),
                config.getfloat('clusters_params', 'min_fc'),
                config.getint('clusters_params', 'connectivity'),
                config.getint('clusters_params', 'min_size'),
                config.getint('clusters_params', 'min_dist')
            )
            folder = os.path.join(folder, clusters_dir)
            if stage in STEP_TO_STAGE[step]:
                return os.path.join(folder, filename)
        elif step == 'collect':
            pass
        else:
            raise ValueError('step %s not recognized' % step)
    raise ValueError('stage %s not found in [steps]' % stage)
