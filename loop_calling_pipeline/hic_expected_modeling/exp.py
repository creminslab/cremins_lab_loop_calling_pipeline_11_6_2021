import os
import multiprocessing
from collections import OrderedDict

import numpy as np
from scipy.signal import convolve

from .mathematics import gmean, zero_nans
from .counts import propagate_nans
from .donut import donut_footprint, lower_left_footprint, horz_footprint, vert_footprint
from .matrix import plot_matrix

from .filenames import format_outfile
from .sparse_matrix import roll_footprint,\
    SparseContactMatrix as SCM


def inner_convolve(args):
    """
    Inner function for parallelizing convolution.

    This function is exposed at the module level, allowing it to be used as an
    argument to ``multiprocessing.Pool.map()``.

    Parameters
    ----------
    args : (np.ndarray, np.ndarray)
        The first array is the matrix to convolve over, the second is the
        weight matrix to convolve over it.

    Returns
    -------
    np.ndarray
        The result of the convolution. Same shape as ``args[0]``.
    """
    return convolve(*args, mode='same')


def dynamic_filter(obs, exp, p=2, w_range=(5, 21, 1), min_ll=16,
                   min_fraction=0.75, include_triu=False):
    """
    Computes ``exp*conv(obs)/conv(exp)`` for the donut and lower left footprints
    with a dynamic w strategy.

    Parameters
    ----------
    obs, exp : np.ndarray
        "Tilted" two-dimensional arrays of observed and one-dimensional expected
        values.
    p : int
        Inner radius to use for the footprints.
    w_range : tuple of int
        Range of outer donut values to try. Should be ``(start, stop, step)``,
        as passed to ``range()``.
    min_ll : int
        If the observed count in the lower left footprint is less than this
        value, ``w`` will be increased.
    min_fraction : float
        If fewer than this fraction of the points in a footprint are finite, the
        returned value will be ``np.nan``.
    include_triu : bool
        Pass True to also compute the triu donut expected.

    Returns
    -------
    (np.ndarray, np.ndarray) or (np.ndarray, np.ndarray, np.ndarray)
        Arrays of donut and lower left filtered expected values, respectively.
        If ``include_triu`` is True, then a third np.ndarray will be included,
        representing the triu donut filter.
    """
    ws = np.arange(*w_range)
    obs, exp = propagate_nans(obs, exp)
    matrices = {
        'obs': zero_nans(obs),
        'exp': zero_nans(exp),
        'nan': np.isfinite(obs).astype(int)
    }
    footprints = OrderedDict([
        ('donut', donut_footprint),
        ('ll', lower_left_footprint)
    ])
    if include_triu:
        footprints.update(triu=lambda x, y: np.triu(donut_footprint(x, y)))

    # compute all convolutions over all matrices across all w
    pool = multiprocessing.Pool(multiprocessing.cpu_count())
    conv = {matrix_name: {
        footprint_name: np.stack(
            pool.map(inner_convolve,
                     ((matrix, roll_footprint(footprint(p, w).T))
                      for w in ws)))
        for footprint_name, footprint in footprints.items()}
        for matrix_name, matrix in matrices.items()}
    pool.close()
    pool.join()

    # compute indices of w to use, from https://stackoverflow.com/a/40373273
    idx = np.argmax(conv['obs']['ll'] > min_ll, axis=0) - \
        np.amin(conv['obs']['ll'] <= min_ll, axis=0)

    # minimum finite values needed for each footprint for each w
    min_n = {footprint_name: np.array(
                 [max(1, np.sum(footprint(p, w)) * min_fraction) for w in ws])
             for footprint_name, footprint in footprints.items()}

    # fancy indexing magic to get the final result we want
    rs = np.arange(obs.shape[0])[:, np.newaxis]
    cs = np.arange(obs.shape[1])[np.newaxis, :]
    return (np.where(
        (conv['nan'][f][idx, rs, cs] >= min_n[f][idx]),
        (conv['obs'][f][idx, rs, cs] /
         conv['exp'][f][idx, rs, cs]) * exp,
        np.nan) for f in footprints.keys())


def dynamic_filter_split(obs, exp, p=2, w_range=(5, 21, 1), p_triu=2, w_triu_range=(5,21,1),min_ll=16,
                   min_fraction=0.75, include_triu=True, include_vert_horz=False):
    """
    Computes ``exp*conv(obs)/conv(exp)`` for the donut and lower left footprints
    with a dynamic w strategy.

    Update to dynamic_filter.  Pass in two different p and w parameter sets for
    triu donut filter covered distances and distances beyond triu.

    Parameters
    ----------
    obs, exp : np.ndarray
        "Tilted" two-dimensional arrays of observed and one-dimensional expected
        values.
    p : int
        Inner radius to use for the footprints.
    p_triu : int
        Inner radius to use for the footprints covered by triu.
    w_range : tuple of int
        Range of outer donut values to try. Should be ``(start, stop, step)``,
        as passed to ``range()``.
    w_triu_range : tuple of int
        Range of outer donut values to try. Should be ``(start, stop, step)``,
        as passed to ``range()``.  Covered by triu
    min_ll : int
        If the observed count in the lower left footprint is less than this
        value, ``w`` will be increased.
    min_fraction : float
        If fewer than this fraction of the points in a footprint are finite, the
        returned value will be ``np.nan``.
    include_triu : bool
        Pass True to also compute the triu donut expected.

    Returns
    -------
    (np.ndarray, np.ndarray) or (np.ndarray, np.ndarray, np.ndarray)
        Arrays of donut and lower left filtered expected values, respectively.
        If ``include_triu`` is True, then a third np.ndarray will be included,
        representing the triu donut filter.
    """

    ws_triu = np.arange(*w_triu_range)
    ws = np.arange(*w_range)
    obs, exp = propagate_nans(obs, exp)
    matrices = {
        'obs': zero_nans(obs),
        'exp': zero_nans(exp),
        'nan': np.isfinite(obs).astype(int)
    }
    footprints = OrderedDict([
        ('donut', donut_footprint),
        ('ll', lower_left_footprint),
    ])
    if include_triu:
        footprints.update(triu=lambda x, y: np.triu(donut_footprint(x, y)))
    
    if include_vert_horz:
        footprints.update(vert = vert_footprint)
        footprints.update(horz = horz_footprint)

    # compute all convolutions over all matrices across all w
    pool = multiprocessing.Pool(multiprocessing.cpu_count())

    conv = {}
    for matrix_name, matrix in matrices.items():
        conv[matrix_name] = {}
        for footprint_name, footprint in footprints.items():
            if (footprint_name != "triu"):
                conv[matrix_name][footprint_name] = np.stack(
                    pool.map(inner_convolve,
                             ((matrix, roll_footprint(footprint(p, w).T))
                              for w in ws)))
            else:
                conv[matrix_name][footprint_name] = np.stack(
                    pool.map(inner_convolve,
                             ((matrix, roll_footprint(footprint(p_triu, w_triu).T))
                              for w_triu in ws_triu)))
    pool.close()
    pool.join()


    # compute indices of w to use, from https://stackoverflow.com/a/40373273
    idx = np.argmax(conv['obs']['ll'] > min_ll, axis=0) - \
        np.amin(conv['obs']['ll'] <= min_ll, axis=0)

    # minimum finite values needed for each footprint for each w
    min_n = {footprint_name: np.array([np.sum(footprint(p, w)) * min_fraction
                                       for w in ws])
             for footprint_name, footprint in footprints.items()}

    # fancy indexing magic to get the final result we want
    rs = np.arange(obs.shape[0])[:, np.newaxis]
    cs = np.arange(obs.shape[1])[np.newaxis, :]
    return (np.where(
        (conv['nan'][f][idx, rs, cs] >= min_n[f][idx]),
        (conv['obs'][f][idx, rs, cs] /
         conv['exp'][f][idx, rs, cs]) * exp,
        np.nan) for f in footprints.keys())

def make_one_dim_exp(obs):
    """
    Constructs a one-dimensional expected matrix.

    Parameters
    ----------
    obs : np.ndarray
        Banded matrix of observed data (rows should correspond to offdiagonals).

    Returns
    -------
    np.ndarray
        The one-dimensional expected matrix. Same size and shape as ``obs``.

    Notes
    -----
    Can be applied to a SparseContactMatrix instance::

        one_dim_exp_scm = SCM.apply(make_one_dim_exp, obs_scm)

    """
    return np.tile(gmean(obs, axis=1), (obs.shape[1], 1)).T


def fuse_matrices(matrix_a, matrix_b, k=10):
    """
    Fuses together two matrices at the k'th row (k-1'th offdiagonal if the
    matrices are banded and nan-padded).

    Parameters
    ----------
    matrix_a, matrix_b : np.ndarray
        The matrices to fuse.
    k : int
        The fused matrix is made from the first k rows of matrix_a and the
        remaining rows of matrix_b.

    Returns
    -------
    np.ndarray
        The fused matrix.

    Notes
    -----
    Can be applied to SparseContactMatrix instances::

        fused_scm = SCM.apply(fuse_matrices, scm_a, scm_b)

    """
    return np.concatenate([matrix_a[:k, :], matrix_b[k:, :]])


def process_exp(args, config, data):
    outfiles = {x: format_outfile(config, args.rep, args.chrom, x)
                for x in ['exp_donut', 'exp_ll', 'exp']}

    if 'obs' not in data:
        data['obs'] = data['current']

    if all(map(lambda x: os.path.exists(x), outfiles.values())):
        print('exp already exists, skipping')
        for k in outfiles.keys():
            data[k] = SCM.load(outfiles[k])
        data['current'] = data['exp']
        return

    obs_scm = data['current']

    print('making exp')
    print(' computing 1D expected model')
    one_dim_exp_scm = SCM.apply(make_one_dim_exp, obs_scm)

    print(' computing donut expected')
    exp_scms = list(SCM.apply(
        dynamic_filter,
        obs_scm, one_dim_exp_scm,
        p=config.getint('exp_params', 'p'),
        w_range=(config.getint('exp_params', 'min_w'),
                 config.getint('exp_params', 'max_w') + 1, 1),
        min_ll=config.getint('exp_params', 'min_lower_left_sum'),
        min_fraction=config.getfloat(
            'exp_params', 'min_filter_fraction'),
        include_triu=(config.get('exp_params', 'fuse_model') == 'triu' and
                      config.getint('exp_params', 'fuse_range'))
    ))
    exp_donut_scm, exp_ll_scm = exp_scms[0:2]
    if len(exp_scms) == 3:
        print(' using fused triu model')
        short_exp_scm = exp_scms[2]
    elif config.get('exp_params', 'fuse_model') == 'one_dim':
        if config.getint('exp_params', 'fuse_range'):
            print(' using fused one dim model')
        else:
            print(' using non-fused model')
        short_exp_scm = one_dim_exp_scm
    else:
        raise ValueError('fuse_model %s not recognized' %
                         config.get('exp_params', 'fuse_model'))
    exp_scm = SCM.max(exp_donut_scm, exp_ll_scm)
    exp_scm = SCM.apply(fuse_matrices, short_exp_scm, exp_scm,
                        k=config.getint('exp_params', 'fuse_range') + 1)

    print(' saving and plotting exp')
    exp_donut_scm.save(outfiles['exp_donut'])
    exp_ll_scm.save(outfiles['exp_ll'])
    exp_scm.save(outfiles['exp'])
    if config.getboolean('matrix', 'plot'):
        plot_matrix(
            exp_donut_scm, log=True, pseudocount=1, outfile=format_outfile(
                config, args.rep, args.chrom, 'exp_donut', ext='png'))
        plot_matrix(exp_ll_scm, log=True, pseudocount=1, outfile=format_outfile(
            config, args.rep, args.chrom, 'exp_ll', ext='png'))
        plot_matrix(exp_scm, log=True, pseudocount=1, outfile=format_outfile(
            config, args.rep, args.chrom, 'exp', ext='png'))

    data['exp_donut'] = exp_donut_scm
    data['exp_ll'] = exp_ll_scm
    data['exp'] = exp_scm
    data['current'] = data['exp']
