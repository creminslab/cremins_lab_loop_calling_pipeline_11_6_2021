import subprocess

chroms =  ['chr%i'% i for i in range(1,23)] + ['chrX']

#adjust resolution to match balanced input files
reskb = 10 #kb
res =10000 

#Allows you to run two sets of files if the are saved in the save file location such as a control and knock down condition. ie ['name_ctrl', 'name_KD'] (must match the name of the input files)
reps = ['H1hESC2.5']

#this saves as part of the name of the file. Can be changed to anything or taken out all together. It is typically used to include parameter values in the name of the file. 
para = 'p2w10_no_triu_VH_minsize_3_mindist_3_minobs_8_conn_1' 

#this is the pathway only but the name of the file is hard coded below in the cmd line
balanced_path = '/pathway/to/balanced/files'
outputfile_path = '/pathway/where/you/want/to/save/expected/files'


for rep in reps:
    for chrom in chroms:
        cmd = f'bsub -M 40000 -o exp.log python expected.py {res} {balanced_path}/{rep}_balanced_{chrom}_{res}.npz {outputfile_path}/{rep}_{chrom}_exp_{para}.npz'
        print(cmd)
        subprocess.call(cmd, shell=True)
